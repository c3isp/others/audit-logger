/**
 *  Copyright 2019 Hewlett Packard Enterprise Development Company, L.P.
 */
package com.hpe.c3isp.audit.logger.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.support.AbstractTlsDirContextAuthenticationStrategy;
import org.springframework.ldap.core.support.DefaultTlsDirContextAuthenticationStrategy;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.pool2.factory.PoolConfig;
import org.springframework.ldap.pool2.factory.PooledContextSource;
import org.springframework.ldap.pool2.validation.DefaultDirContextValidator;
import org.springframework.ldap.transaction.compensating.manager.TransactionAwareContextSourceProxy;
import org.springframework.security.authentication.encoding.LdapShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * @author MIMANE
 *
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Value("${security.activation.status}")
	private boolean securityActivationStatus;
	
	@Value("${security.user.name}")
	private String builtinUserName;

	@Value("${security.user.password}")
	private String builtinPassword;
	
    @Value("${ldap.userSearchBase}")
    private String ldapUserSearchBase;

    @Value("${ldap.userSearchFilter}")
    private String ldapUserSearchFilter;

    @Value("${ldap.url}")
    private String ldapUrl;

    @Value("${ldap.use.starttls}")
    private boolean useStartTls;
    
    @Autowired
    LdapContextSource contextSource;
    
	/**
	 * These paths are required for Swagger and so must not be protected
	 */
	String[] apiPath = {
			"/v2/api-docs",
			"/configuration/ui",
			"/swagger-resources",
			"/configuration/security",
			"/swagger-ui.html",
			"/webjars/**"
	};
	
	/**
	 * Here we configure basic authentication for REST endpoints starting with '/v1/' path
	 */
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		System.out.println("securityActivationStatus=" + securityActivationStatus);
		if (!securityActivationStatus)
			http.authorizeRequests().anyRequest().permitAll();
		else {
        http.httpBasic();
        http.authorizeRequests()
    			.antMatchers(apiPath).permitAll()
    			.antMatchers("/v1/**").authenticated();
		}
        http.csrf().disable();
        http.headers().frameOptions().disable();
    }

    /**
     * Config LDAP authentication
     */
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        
        auth
            .inMemoryAuthentication()
                .withUser(builtinUserName)
                .password(builtinPassword)
                .roles("C3ISP")
        ;

        auth
            .ldapAuthentication()
                //.userDnPatterns("cn={0},ou=Users")
                //.userSearchBase(ldapUserSearchBase)
                .userSearchFilter(ldapUserSearchFilter)
                .contextSource(contextSource)
                    //.url(ldapUrl)
                    //.and()
                    //.passwordEncoder(new LdapShaPasswordEncoder())
                    //.passwordAttribute("userPassword");
        ;
    }
    
    @Bean
    public LdapContextSource ldapContextSource() {
        LdapContextSource contextSource = new LdapContextSource();
        contextSource.setUrl(ldapUrl);
        contextSource.setBase(ldapUserSearchBase);
        contextSource.setPooled(false);
        if (useStartTls) {
            DefaultTlsDirContextAuthenticationStrategy strategy = new DefaultTlsDirContextAuthenticationStrategy();
            strategy.setShutdownTlsGracefully(true);
            contextSource.setAuthenticationStrategy(strategy);
        }

        return contextSource;
    }

    /*@Bean
    public ContextSource poolingLdapContextSource() {
        PoolConfig config = new PoolConfig();
        config.setTestOnBorrow(true);
        config.setTestWhileIdle(true);
        PooledContextSource poolingContextSource = new PooledContextSource(config);
        poolingContextSource.setDirContextValidator(new DefaultDirContextValidator());
        poolingContextSource.setContextSource(ldapContextSource());

        TransactionAwareContextSourceProxy proxy = new TransactionAwareContextSourceProxy(poolingContextSource);
        return proxy;
    }*/
    
}
