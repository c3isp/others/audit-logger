package com.hpe.c3isp.audit.logger.restapi.types;

/**
 * @author HPE This class models the json event sent by log4j2 when using the
 *         <JsonLayout /> pattern layout
 */
public class LogEvent {

    /**
     * This reports when (in millis) the event took place on the originating system
     */
    private long timeMillis;
    private String thread;
    private String level;
    private String loggerName;
    private String message;
    private boolean endOfBatch;
    private String loggerFqcn;

    /**
     * Empty constructor used for jackson marshalling
     */
    public LogEvent() {

    }

    /**
     * @return the timeMillis
     */
    public long getTimeMillis() {
        return timeMillis;
    }

    /**
     * @param timeMillis
     *            the timeMillis to set
     */
    public void setTimeMillis(long timeMillis) {
        this.timeMillis = timeMillis;
    }

    /**
     * @return the thread
     */
    public String getThread() {
        return thread;
    }

    /**
     * @param thread
     *            the thread to set
     */
    public void setThread(String thread) {
        this.thread = thread;
    }

    /**
     * @return the level
     */
    public String getLevel() {
        return level;
    }

    /**
     * @param level
     *            the level to set
     */
    public void setLevel(String level) {
        this.level = level;
    }

    /**
     * @return the loggerName
     */
    public String getLoggerName() {
        return loggerName;
    }

    /**
     * @param loggerName
     *            the loggerName to set
     */
    public void setLoggerName(String loggerName) {
        this.loggerName = loggerName;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the endOfBatch
     */
    public boolean isEndOfBatch() {
        return endOfBatch;
    }

    /**
     * @param endOfBatch
     *            the endOfBatch to set
     */
    public void setEndOfBatch(boolean endOfBatch) {
        this.endOfBatch = endOfBatch;
    }

    /**
     * @return the loggerFqcn
     */
    public String getLoggerFqcn() {
        return loggerFqcn;
    }

    /**
     * @param loggerFqcn
     *            the loggerFqcn to set
     */
    public void setLoggerFqcn(String loggerFqcn) {
        this.loggerFqcn = loggerFqcn;
    }

}
