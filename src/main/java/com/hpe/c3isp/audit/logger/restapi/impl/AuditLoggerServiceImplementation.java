/**
 *  Copyright 2019 Hewlett Packard Enterprise Development Company, L.P.
 */
package com.hpe.c3isp.audit.logger.restapi.impl;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.hpe.c3isp.audit.logger.restapi.types.LogEvent;

@ApiModel(value = "AuditLogger", description = "Public methods of Audit Logger")
@RestController
@RequestMapping("/v1")
public class AuditLoggerServiceImplementation {

    /**
     * These two are examples of configuration extracted from the
     * application.properties file under the src/main/resources. Springboot will
     * automatically load the file and its values, if file is not in the local path,
     * it can be in the java classpath or set as an environment variable
     */
    @Value("${security.user.name}")
    private String restUser;
    @Value("${security.user.password}")
    private String restPassword;

    /**
     * Used to call REST endpoints; configured by RestTemplateBuilder
     */
    @Autowired
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
    }

    /**
     * Logger class, can be used within the methods to log information
     */
    private final static Logger LOGGER = LoggerFactory.getLogger(AuditLoggerServiceImplementation.class);

    private final static Logger AUDIT = LoggerFactory.getLogger("AuditLog");

    @Value("${audit.log.cef.enable.origTime}")
    private boolean enableCefEndTime;

    /**
     * auditlog REST service
     * 
     * @param cef
     *            an object embedding the CEF event received in a JsonLayout log4j2
     *            structure
     * @return a Boolean
     */
    @ApiOperation(httpMethod = "POST",
            value = "Sends an auditlog to the Log Management service",
            notes = "")
    @ApiResponses(value = { @ApiResponse(code = 400, message = "Client Error"),
            @ApiResponse(code = 403, message = "Authorization error"),
            @ApiResponse(code = 500, message = "Internal server error") })
    @RequestMapping(method = RequestMethod.POST,
            value = "/auditlog/",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> auditLog(
            @ApiParam(name = "CEF wrapper object",
                      value = "Currently only <code>message</code> and <code>timeMillis</code> fields are used.\nExample:\n" +
                              "<code>{\r\n" + 
                              "  \"endOfBatch\": true,\r\n" + 
                              "  \"level\": \"INFO\",\r\n" + 
                              "  \"loggerFqcn\": \"com.hpe.c3isp.logger.auditlog\",\r\n" + 
                              "  \"loggerName\": \"auditlog\",\r\n" + 
                              "  \"message\": \"CEF:0|C3ISP|\\/TEST_MSG|1.0.0|GET|Response to GET on \\/TEST_MSG\\/|9|app=HTTP\\/1.1 msg=GET on \\/test-service\\/ reason=200 request=https:\\/\\/isic3isp.iit.cnr.it\\/TEST_MSG\\/v1\\/test dst=127.0.1.1 src=127.0.0.1 requestMethod=POST deviceDirection=1 dhost=ISIC3ISP.iit.cnr.it outcome=Success shost=127.0.0.1\",\r\n" + 
                              "  \"thread\": \"http-nio-8443-exec-71\",\r\n" + 
                              "  \"timeMillis\": 1555673227366\r\n" + 
                              "}</code>",
                      required = true)
            @RequestBody() LogEvent cef) {
        LOGGER.debug("Received POST request");

        // LOGGER.debug("AuditLog CEF=" + cef.toString());
        LOGGER.debug("AuditLog CEF=" + cef.getMessage());

        ResponseEntity<Boolean> response;
        Boolean success = false;

        // Call to syslog component and register the log
        // Add to log the timeMillis and set it to end (endTime)
        // TODO: verify if it would be better do use 'deviceCustomDate1' instead of
        // 'end'
        try {
            if ((enableCefEndTime) && (cef.getTimeMillis() != 0)) {
                AUDIT.info(cef.getMessage() + " end=" + cef.getTimeMillis());
            } else {
                AUDIT.info(cef.getMessage());
            }
            success = true;
            response = new ResponseEntity<Boolean>(success, HttpStatus.OK);
        } catch (Exception e) {
            response = new ResponseEntity<Boolean>(success, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }

}
