/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package com.hpe.c3isp.audit.logger.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.hpe.c3isp.audit.logger.ApplicationDeployer;

import io.github.swagger2markup.Swagger2MarkupConfig;
import io.github.swagger2markup.Swagger2MarkupConverter;
import io.github.swagger2markup.builder.Swagger2MarkupConfigBuilder;

/**
 * @author MIMANE
 */
/**
 * These tests are used to generate the ASCIIDOC documentation for the API.
 * From Maven pom.xml you can decide to generate PDF or HTML starting from this ASCIIDOC
 * Skip them, if you don't need it.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApplicationDeployer.class)
public class Swagger2MarkupTest {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void convertSwaggerToAsciiDoc() throws Exception {
//		this.mockMvc.perform(get("/v2/api-docs").accept(MediaType.APPLICATION_JSON))
//		    	.andDo(Swagger2MarkupResultHandler.outputDirectory("target/docs/asciidoc/generated").build())
//				.andExpect(status().isOk());
		MvcResult result = this.mockMvc.perform(get("/v2/api-docs")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();
		System.out.println("Ret JSON: "+result.getResponse().getContentAsString());
		
		Swagger2MarkupConfig config = new Swagger2MarkupConfigBuilder()
				.withGeneratedExamples()
				.build();
		
		Swagger2MarkupConverter.from(result.getResponse().getContentAsString())
				.withConfig(config)
				.build()
				.toFolder(Paths.get("target/docs/asciidoc/generated"));
	}

//	@Test
//	public void convertSwaggerToMarkdown() throws Exception {
//		this.mockMvc.perform(get("/v2/api-docs").accept(MediaType.APPLICATION_JSON)).andDo(Swagger2MarkupResultHandler
//				.outputDirectory("target/docs/markdown/generated").withMarkupLanguage(MarkupLanguage.MARKDOWN).build())
//				.andExpect(status().isOk());
//	}

}
