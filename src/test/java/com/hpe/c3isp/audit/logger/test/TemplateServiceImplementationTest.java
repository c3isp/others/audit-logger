/**
 *  Copyright 2019 Hewlett Packard Enterprise Development Company, L.P.
 */
package com.hpe.c3isp.audit.logger.test;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

// for code completion add MockMvcRequestBuilders and MockMvcRequestBuilders as 'favorite types' in eclipse
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*; //get, post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;

import static org.hamcrest.CoreMatchers.*;

/**
 * @author MIMANE
 *
 */
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ActiveProfiles("test") // load application-test.properties
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TemplateServiceImplementationTest {

    @Autowired
    private MockMvc mockMvc;
    
    @Value("${security.user.name}")
    private String restUser;
    @Value("${security.user.password}")
    private String restPassword;

	@Test
    public void test01post() throws Exception {
        
        String cefLog = "{\r\n" + 
                "  \"endOfBatch\": true,\r\n" + 
                "  \"level\": \"string\",\r\n" + 
                "  \"loggerFqcn\": \"string\",\r\n" + 
                "  \"loggerName\": \"string\",\r\n" + 
                "  \"message\": \"INFO: CEF:0|C3ISP|\\/TEST_MSG|1.0.0|GET|Response to GET on \\/test-service\\/|9|app=HTTP\\/1.1 msg=GET on \\/test-service\\/ reason=200\",\r\n" + 
                "  \"thread\": \"string\",\r\n" + 
                "  \"timeMillis\": 1555673227366\r\n" + 
                "}";
        String expectedOutput = "true";
        
        System.out.println(">>>>>>>>>>>"+restUser);
        this.mockMvc.perform(
                post("/v1/auditlog/")
                    .with(httpBasic(restUser, restPassword)) // basic auth
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(cefLog)
                 )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(expectedOutput))) //check if the returned output (json) contains 'expectedOutput
                ;
    }
	
	/*@Test
    public void test02get() throws Exception {
        
        String param = "test";
        //expected output structure = "{\"name\":\"aName\",\"id\":\"anIdValue\",\"path\":\"aPath\",\"version\":\"1\"}";
        
        this.mockMvc.perform(
                get("/v1/template/" + param + "/")
                    .with(httpBasic(restUser, restPassword)) // basic auth
                    .header("X-myheader-test1", "TEST1")
                    .header("X-myheader-test2", "TEST2")
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                 )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("aName")))
                ;
        
    }*/
}
